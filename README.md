# SOURCE CODE OF MY PORTFOLIO

Here you can find _the entiere_ code hosted on my server.

You are __free__ and even invited to use it __in any way you want__.
If you do, I'd rather be told about it.
It is __not mandatory__, but who knows ? Maybe we could do things __together__ !

Appreciating my work ? Don't hesitate to [contact me](https://mael_garrigues.pw).

## Not so entiere...

Of course some things are hidden for _security and privacy_ reasons.

### Stuffs I still have to do:

---
---
- [x] test Markdown
---
---
- [ ] structure html (php)
---
---
- [ ] structure css
---
---
- [ ] structure js
---
---
  - [ ] FILL
  ---
  - [ ] about
  ---
  - [ ] design
  ---
  - [ ] project
  ---
---
---
- [ ] get a server and...
---
---
- [ ] solve issues
---
---
- [ ] __enjoy__
---
---
- [ ] and then _animate_...
---
---
- [ ] ___really enjoy !!!___
---
---
- [ ] update
---
---